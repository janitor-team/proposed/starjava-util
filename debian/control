Source: starjava-util
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: java
Priority: optional
Build-Depends: ant,
               debhelper-compat (= 13),
               dh-exec,
               javahelper
Build-Depends-Indep: ant-optional,
                     default-jdk,
                     default-jdk-doc,
                     junit
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/starjava-util
Vcs-Git: https://salsa.debian.org/debian-astro-team/starjava-util.git
Homepage: https://github.com/Starlink/starjava/tree/master/util
Rules-Requires-Root: no

Package: starlink-util-java
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends}
Breaks: starlink-table-java (<< 4.1~),
        starlink-topcat-java (<< 4.8.4~),
        starlink-ttools-java (<< 3.4.4~),
        starlink-votable-java (<< 0.2+2022.04.04~)
Multi-Arch: foreign
Description: Miscellaneous utilities for the Starjava classes
 This package is a helper function for the other Starjava packages. It
 contains a number of helper and utility classes that don't fit elsewhere.
 .
 Classes include some basic functionality like lists for Java
 primitives, input/output classes, gui classes, DTD and xsd etc.

Package: starlink-util-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Recommends: ${java:Recommends}
Description: Miscellaneous utilities for the Starjava classes (documentation)
 This package is a helper function for the other Starjava packages. It
 contains a number of helper and utility classes that don't fit elsewhere.
 .
 Classes include some basic functionality like lists for Java
 primitives, input/output classes, gui classes, DTD and xsd etc.
 .
 This package contains the JavaDoc documentation of the package.
